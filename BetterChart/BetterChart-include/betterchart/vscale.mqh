/*
Copyright 2024 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// BetterChart. Vertical Scale. © FXcoder

#include "../chart.mqh"
#include "../chartevent.mqh"
#include "../gv.mqh"
#include "../series.mqh"
#include "../terminal.mqh"
#include "../enum/mtkey.mqh"
#include "betterchartmodule.mqh"

class CVScale: public CBetterChartModule
{
private:

	ushort key_;
	double percent_;

	bool fixed_scale_;
	bool force_scale_range_update_;
	CGV  gv_fixed_scale_;
	const string gv_prefix_;

public:

	void CVScale(ushort key, double percent):
		CBetterChartModule(),
		key_(key),
		percent_(percent),
		fixed_scale_(false),
		force_scale_range_update_(true),
		gv_prefix_("fxcoder.betterchart.fixedscale.")
	{
		gv_fixed_scale_.name(gv_prefix_ + (string)_chart.id());
	}

	virtual int init() override
	{
		fixed_scale_ = gv_fixed_scale_.get_or_default(0);

		if (fixed_scale_)
			_chart.scale_fix(true);

		return INIT_SUCCEEDED;
	}

	virtual void deinit(int reason) override
	{
		for (int i = GlobalVariablesTotal() - 1; i >= 0; --i)
		{
			CGV gv(i);
			if (StringFind(gv_prefix_, gv.name()) == 0 && gv.get_or_default(0) == 0)
				gv.del();
		}
	}

	virtual void chart_event() override
	{
		bool fix = _chart.scale_fix();

		if (_chartevent.is_chart_change_event())
		{
			if (fix)
				update_scale();
		}
		else if (_chartevent.is_key_down_event(key_))
		{
			fix = !fix;

			if (_terminal.is_shift_key_pressed())
			{
				for (long chart_id = ChartFirst(); chart_id >= 0; chart_id = ChartNext(chart_id))
				{
					CChart chart(chart_id);
					update_fix_scale(fix, chart);
				}
			}
			else
			{
				update_fix_scale(fix, _chart);
			}

			fixed_scale_ = fix;
			if (fixed_scale_)
				gv_fixed_scale_.set(true);
			else
				gv_fixed_scale_.del();
		}
	}

private:

	void update_fix_scale(bool fix_scale, CChart &chart)
	{
		chart.scale_fix(fix_scale);
		update_scale(chart.id());

		// в здесь 5 требуется принудительное обновление (1816)
		if (!fix_scale)
			chart.redraw();
	}

	void update_scale(long chart_id = 0)
	{
		CChart chart(chart_id);

		// Определить диапазон цен на графике
		int left_bar = chart.leftmost_visible_bar();
		int right_bar = chart.rightmost_visible_bar(true);
		int nbars = left_bar - right_bar + 1;

		double highs[], lows[];
		CSeries series(chart.symbol(), chart.period());
		int high_bars = series.copy_high(right_bar, nbars, highs);
		int low_bars  = series.copy_low(right_bar, nbars, lows);

		bool changed = false;

		if (high_bars == nbars && low_bars == nbars)
		{
			const double price_max = _math.max(highs);
			const double price_min = _math.min(lows);
			const double price_range = price_max - price_min;


			const double win_range = price_range / percent_ * 100.0;
			const double win_center = price_min + price_range / 2.0;

			const double new_win_min = NormalizeDouble(win_center - win_range / 2.0, _Digits);
			const double new_win_max = NormalizeDouble(win_center + win_range / 2.0, _Digits);

			const double old_win_max = chart.fixed_max();
			const double old_win_min = chart.fixed_min();

			const double old_range = old_win_max - old_win_min;
			const double new_range = new_win_max - new_win_min;

			// Игнорировать малые изменения (против дребезга)
			if (force_scale_range_update_ || (fabs(new_range - old_range) > 10.0 * _Point))
			{
				changed = true;
				chart.fixed_max(new_win_max);
				chart.fixed_min(new_win_min);
				force_scale_range_update_ = false;
			}
		}
		if (changed)
			chart.redraw();
	}
};
