/*
Copyright 2024 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// BetterChart. Alternative Vertical Shift. © FXcoder

#include <Generic/Stack.mqh>
#include "../enum/mtkey.mqh"
#include "../chart.mqh"
#include "../chartevent.mqh"
#include "../go.mqh"
#include "../series.mqh"
#include "betterchartmodule.mqh"

class CObjMove: public CBetterChartModule
{
private:

	const ushort move_left_key_;
	const ushort move_right_key_;
	const ushort event_n_;

	CStack<string> selected_;

public:

	void CObjMove(ushort move_left_key, ushort move_right_key, ushort event_n):
		CBetterChartModule(),
		move_left_key_(move_left_key),
		move_right_key_(move_right_key),
		event_n_(event_n)
	{
	}

	virtual void chart_event() override
	{
		update_selected();

		if (!_chartevent.is_key_down_event())
			return;

		const ushort key = _chartevent.key();
		if (key != move_left_key_ && key != move_right_key_)
			return;

		// find existing selected object
		string obj_name = "";
		while (selected_.Count() > 0)
		{
			CGO obj(selected_.Peek());
			if (obj.exists() && obj.selected())
			{
				obj_name = obj.name();
				break;
			}

			selected_.Pop();
		}

		// move
		if (obj_name != "")
		{
			CGO go(obj_name);
			int bar = _series.bar_shift_left(go.time());

			bar += key == move_left_key_ ? +1 : -1;

			go.time(_series.time(bar, false, true), 0);
			_chart.redraw();

			_chart.event_custom(event_n_, 0, 0, obj_name);
		}
	}

private:

	void update_selected()
	{
		/*
		1. Check if user click, drag object or create one. Put it on top if selected or forget otherwise.
		2. Forget the deleted object.
		3. If nothing selected manually, try to find all selected ordered by
			creation order.

		OBJ_HLINE is the only one object type that does not have time property.
		*/

		if (_chartevent.is_object_event())
		{
			CGO obj(_chartevent.sparam());
			if (obj.type() == OBJ_HLINE || !obj.selectable())
				return;

			// change last selected object
			if (_chartevent.is_object_click_event() ||
				_chartevent.is_object_create_event() ||
				_chartevent.is_object_drag_event())
			{
				selected_.Remove(obj.name());
				if (obj.selected())
					selected_.Push(obj.name());
			}
			else if (_chartevent.is_object_delete_event())
			{
				selected_.Remove(obj.name());
			}
		}

		if (selected_.Count() != 0)
			return;

		// if nothing is selected manually, find all selected
		for (int i = 0, count = _chart.objects_total(); i < count; ++i)
		{
			CGO obj(_chart.object_name(i));
			if (obj.type() == OBJ_HLINE)
				continue;

			if (obj.selected())
				selected_.Push(obj.name());
		}
	}
};
