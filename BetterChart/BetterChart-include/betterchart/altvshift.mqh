/*
Copyright 2024 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// BetterChart. Alternative Vertical Shift. © FXcoder

#include "../chart.mqh"
#include "../chartevent.mqh"
#include "../terminal.mqh"
#include "../enum/mtkey.mqh"
#include "betterchartmodule.mqh"

class CAltVShift: public CBetterChartModule
{
public:

	void CAltVShift():
		CBetterChartModule()
	{
	}

	virtual void chart_event() override
	{
		if (!_chart.scale_fix())
			return;

		if (!_chartevent.is_key_down_event(MTKEY_DOWN) && !_chartevent.is_key_down_event(MTKEY_UP))
			return;

		double max = _chart.fixed_max();
		double min = _chart.fixed_min();

		double height = max - min;
		if (height <= 0)
			return;

		if (_terminal.is_control_key_pressed() && _terminal.is_shift_key_pressed())
			return;

		double shift = _terminal.is_shift_key_pressed()
			? height / 20.0
			: height / 200.0;

		if (_chartevent.key() == MTKEY_DOWN)
			shift = -shift;

		shift = NormalizeDouble(shift, _Digits);

		_chart.fixed_max(max + shift);
		_chart.fixed_min(min + shift);
		_chart.redraw();
	}
};
