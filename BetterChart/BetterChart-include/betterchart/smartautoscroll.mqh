/*
Copyright 2024 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// BetterChart. Smart Autoscroll. © FXcoder

#include "../chart.mqh"
#include "../chartevent.mqh"
#include "betterchartmodule.mqh"

class CSmartAutoscroll: public CBetterChartModule
{
private:

	int  prev_scale_;
	int  prev_last_visible_bar_;

public:

	void CSmartAutoscroll():
		CBetterChartModule()
	{
		prev_scale_ = _chart.scale();
		prev_last_visible_bar_ = _chart.rightmost_visible_bar(false);
	}

	virtual void chart_event() override
	{
		if (!_chartevent.is_chart_change_event())
			return;

		int last_visible_bar = _chart.rightmost_visible_bar(false);
		int scale = _chart.scale();

		// Включить автосдвиг, если в текущей ситуации виден последний бар, либо если
		// был изменён масштаб графика и при этом раньше последний бар был видим.
		bool enable_autoscroll = (last_visible_bar < 0) || ((prev_scale_ != scale) && (prev_last_visible_bar_ < 0));
		_chart.autoscroll(enable_autoscroll);

		prev_last_visible_bar_ = last_visible_bar;
		prev_scale_ = scale;
	}
};
