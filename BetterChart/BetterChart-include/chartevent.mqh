/*
Copyright 2024 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// OnChartEvent helper. © FXcoder

#include "math.mqh"

class CChartEvent
{
private:

	int    id_;
	long   lparam_;
	double dparam_;
	string sparam_;

	// время предыдущего клика в мс для определения двойного клика
	long            chart_dbl_click_prev_click_time_msc_;
	string          chart_dbl_click_prev_symbol_;
	ENUM_TIMEFRAMES chart_dbl_click_prev_period_;

public:

	long   lparam() const { return lparam_; }
	double dparam() const { return dparam_; }
	string sparam() const { return sparam_; }

	void update(const int id, const long &lparam, const double &dparam, const string &sparam)
	{
		id_ = id;
		lparam_ = lparam;
		dparam_ = dparam;
		sparam_ = sparam;
	}
	bool is_chart_change_event() const { return id_ == CHARTEVENT_CHART_CHANGE; }

	bool is_custom_event()
	{
		return _math.is_in(id_, (int)CHARTEVENT_CUSTOM, (int)CHARTEVENT_CUSTOM_LAST + 1);
	}

	bool is_custom_event(int event_n) const
	{
		const int check_id = CHARTEVENT_CUSTOM + event_n;
		return check_id == id_ && _math.is_in(check_id, (int)CHARTEVENT_CUSTOM, (int)CHARTEVENT_CUSTOM_LAST + 1);
	}

	bool is_object_create_event() const
	{
		return id_ == CHARTEVENT_OBJECT_CREATE;
	}

	bool is_object_drag_event() const
	{
		return id_ == CHARTEVENT_OBJECT_DRAG;
	}

	bool is_object_delete_event() const
	{
		return id_ == CHARTEVENT_OBJECT_DELETE;
	}

	// подразумеваются все действия, изменяющие координаты, включая создание и удаление объекта
	bool is_object_event() const
	{
		return
			id_ == CHARTEVENT_OBJECT_CREATE ||
			id_ == CHARTEVENT_OBJECT_CHANGE ||
			id_ == CHARTEVENT_OBJECT_DRAG   ||
			id_ == CHARTEVENT_OBJECT_DELETE ||
			id_ == CHARTEVENT_OBJECT_ENDEDIT ||
			id_ == CHARTEVENT_OBJECT_CLICK;
	}

	// подразумеваются все действия, изменяющие координаты, включая создание и удаление объекта
	bool is_object_event(string name) const
	{
		return is_object_event() && sparam_ == name;
	}

	bool is_key_down_event() const
	{
		return id_ == CHARTEVENT_KEYDOWN;
	}

	bool is_key_down_event(ushort key) const
	{
		return is_key_down_event() && key == lparam_;
	}

	bool is_object_click_event()
	{
		return id_ == CHARTEVENT_OBJECT_CLICK;
	}
	ushort key() const
	{
		return (ushort)lparam_;
	}

} _chartevent;
