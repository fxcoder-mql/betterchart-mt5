/*
Copyright 2024 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "BetterChart 5.0. © FXcoder"
#property link      "https://fxcoder.blogspot.com"
#property indicator_chart_window
#property indicator_plots 0


#include <Generic/ArrayList.mqh>
#include "BetterChart-include/enum/mtkey.mqh"
#include "BetterChart-include/betterchart/smartautoscroll.mqh"
#include "BetterChart-include/betterchart/oneclicktrading.mqh"
#include "BetterChart-include/betterchart/copychart.mqh"
#include "BetterChart-include/betterchart/altvshift.mqh"
#include "BetterChart-include/betterchart/objmove.mqh"
#include "BetterChart-include/betterchart/vscale.mqh"


input bool       SmartAutoscroll       = true;               // Smart Autoscroll
input ENUM_MTKEY OneClickTradingKey    = MTKEY_GRAVE_ACCENT; // One Click Trading Panel Key (None = disable)
input ENUM_MTKEY CopyChartKey          = MTKEY_D;            // Copy Chart Key (with Shift key, None = disable)
input bool       AlternativeVShift     = true;               // Alternative Vertical Shift
input bool       ObjMove               = true;               // Object Move
input ENUM_MTKEY ObjMoveLeft           = MTKEY_COMMA;        // Object Move Left Key
input ENUM_MTKEY ObjMoveRight          = MTKEY_DOT;          // Object Move Right Key
input ENUM_MTKEY VScaleKey             = MTKEY_I;            // Vertical Scale Button (None = disable)
input double     VScalePercent         = 61.8;               // Vertical Scale Percent (1..200)

CArrayList<CBetterChartModule*> modules_;


int OnInit()
{
	if (SmartAutoscroll)
		modules_.Add(new CSmartAutoscroll());

	if (OneClickTradingKey != MTKEY_NONE)
		modules_.Add(new COneClickTradingKey(OneClickTradingKey));

	if (CopyChartKey != MTKEY_NONE)
		modules_.Add(new CCopyChart(CopyChartKey));

	if (AlternativeVShift)
		modules_.Add(new CAltVShift());

	if (ObjMove)
		modules_.Add(new CObjMove(ObjMoveLeft, ObjMoveRight, 1));

	if (VScaleKey != MTKEY_NONE)
		modules_.Add(new CVScale(VScaleKey, _math.clamp(VScalePercent, 1.0, 200.0)));

	for (int i = modules_.Count() - 1; i >= 0; --i)
	{
		CBetterChartModule* module;
		if (modules_.TryGetValue(i, module))
		{
			int init_res = module.init();
			if (init_res != INIT_SUCCEEDED)
				return init_res;
		}
	}

	return INIT_SUCCEEDED;
}

void OnDeinit(const int reason)
{
	for (int i = modules_.Count() - 1; i >= 0; --i)
	{
		CBetterChartModule* module;
		if (modules_.TryGetValue(i, module))
			delete module;
	}
}

void OnChartEvent(const int id, const long &lparam, const double &dparam, const string &sparam)
{
	_chartevent.update(id, lparam, dparam, sparam);

	for (int i = modules_.Count() - 1; i >= 0; --i)
	{
		CBetterChartModule* module;
		if (modules_.TryGetValue(i, module))
			module.chart_event();
	}
}

int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
	return rates_total;
}
